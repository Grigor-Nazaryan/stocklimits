sequelize model:generate --name Project --attributes platform:string - create db

sequelize db:migrate:undo:all - delete db

sequelize db:migrate - migrate updated models