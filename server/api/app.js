import express from 'express';
import bodyParser from 'body-parser';
import authRoute from './routes/authRoute'
import projectRoute from './routes/projectRoute'

// import passport from 'passport'
// import cookieParser from 'cookie-parser'
// import session from 'express-session'

const app = express();
const PORT = process.env.PORT || 8500

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

// app.use(cookieParser());

// require('./db/config/passport')
// app.use(passport.initialize());



// Use Routes
app.use('/api/auth', authRoute)
app.use('/api', projectRoute)

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))