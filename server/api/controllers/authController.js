import bcrypt from 'bcryptjs';
import { User } from '../db/models';
import uuidv4 from 'uuid/v4';
import jwt from '../middleware/jwt';
import { sendEmail } from '../utilities/mailgun';
//const passport = require('passport');

/**
 * @description Authentication Controller
 * @class authController
 */

class authController {
  /**
   * @description Registration method
   * @static
   * @param {object} req
   * @param {object} res
   * @returns {object} User
   * @memberof authController
   */
  static async register(req, res) {
    try {
      const { name, lastname, email, password, isVerified } = req.user;
      const isEmailExist = await User.findOne({
        where: {
          email
        }
      });

      if (isEmailExist) {
        return res.status(409).json({
          status: 409,
          message: 'Email already exists'
        });
      }

      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(password, salt);

      const isUser = await User.create({
        userId: uuidv4(),
        name,
        lastname,
        email,
        password: hash,
        //isVerified
      });

      const user = {};
      user.user = {};
      user.user.userId = isUser.userId;
      user.user.name = isUser.name;
      user.user.lastname = isUser.lastname;
      user.user.email = isUser.email;
      //user.user.isVerified = isUser.isVerified;
      const token = await jwt.generateToken(user);

      // await sendEmail(
      //   user.email,
      //   'stock@gmail.com',
      //   'Hello there',
      //   req.body.message
      // );

      return res.status(201).json({
        status: 201,
        message: user,
        token
      });
    } catch (error) {
      return res.status(500).json({
        status: 500,
        message: error.message
      });
    }
  }

  /**
   * @description Login method
   * @static
   * @param {object} req
   * @param {object} res
   * @returns {object} User
   * @memberof authController
   */
  static async login(req, res) {
    try {
      const { email, password } = req.user;
      const isEmailExist = await User.findOne({
        where: {
          email
        },
        raw: true,
        attributes: { exclude: ['createdAt', 'updateAt'] }
      });

      if (!isEmailExist) {
        return res.status(404).json({
          status: 404,
          message: 'Email does not exists'
        });
      }

      const comparePassword = bcrypt.compareSync(
        password,
        isEmailExist.password
      );

      if (!comparePassword) {
        return res.status(400).json({
          status: 400,
          message: 'Invalid Password'
        });
      }

      const user = {};
      user.user = {};
      user.user.userId = isEmailExist.userId;
      user.user.name = isEmailExist.name;
      user.user.lastname = isEmailExist.lastname;
      user.user.email = isEmailExist.email;
      user.user.isVerified = isEmailExist.isVerified;
      const token = await jwt.generateToken(user);

      return res.status(201).json({
        status: 201,
        message: user,
        token
      });
    } catch (error) {
      return res.status(500).json({
        status: 500,
        message: error.message
      });
    }
  }

  /**
   * @description Edit User
   * @static
   * @param {object} req
   * @param {object} res
   * @returns {object} User
   * @memberof authController
   */
  static async updateUser(req, res) {
    try {
      const { userId } = req.params;
      const { name, lastname, email, password } = req.user;

      await User.update(
        {
          name,
          lastname,
          email,
          password
        },
        {
          where: {
            userId
          }
        }
      );

      return res.status(201).json({
        status: 201,
        data: 'User has been updated'
      });
    } catch (err) {
      return res.status(500).json({
        status: 500,
        message: err.message
      });
    }
  }
}
export default authController;

// router.get('/confirm/:confirmKey', (req, res) => {
//   if (req.isAuthenticated()) {
//     return res.status(400).send({ message: 'Already logged in' });
//   }

//   models.User.findOne({
//     where: { activationKey: req.params.confirmKey, verified: false }
//   })
//     .then(user => {
//       user
//         .update({ verified: true, activationKey: null })
//         .then(() => {
//           activateOrder(user);
//           return res.status(200).send({ message: 'Activation success' });
//         })
//         .catch(error => {
//           return res.status(400).send(error);
//         });
//     })
//     .catch(error => {
//       return res.status(400).send(error);
//     });
// });

// router.post('/register', (req, res) => {
//   if (req.isAuthenticated()) {
//     return res.status(400).send({ message: 'Already logged in' });
//   }

//   models.User.register(req.body, req.body.password, (err, user) => {
//     let mailer = res.locals.mailer;
//     let config = res.locals.app.get('config');
//     let dev_mode = res.locals.app.get('env') === 'development';
//     let confirmUrl = config.frontend.confirmation_email_uri;

//     if (err !== null) {
//       return res.status(400).send(err);
//     } else {
//       // addOrder(user);

//       let mailer = res.locals.mailer;
//       confirmUrl = confirmUrl + user.activationKey;
//       mailer.send(
//         'email_confirmation',
//         {
//           to: dev_mode ? config.frontend.debug_mail : user.email,
//           subject: 'A Daily Clock account confirmation',
//           link: confirmUrl
//         },
//         err => {
//           if (err) {
//             console.log(err);
//             return res.status(400).send('There was an error sending the email');
//           }
//           let serial = uuid.v1();

//           models.Catalog.findOne({ where: { membership: 'time' } }).then(
//             catalog => {
//               models.Product.build({ serial: serial })
//                 .save()
//                 .then(product => {
//                   let title =
//                     'Order to product with serial number ' + product.serial;
//                   models.Order.build({
//                     title: title,
//                     product_id: product.id,
//                     user_id: user.id,
//                     catalog_id: catalog.id,
//                     amount: catalog.price
//                   })
//                     .save()
//                     .then(order => {
//                       return res.status(200).send(order);
//                     });
//                 })
//                 .catch(err => {
//                   return res.status(400).send(err);
//                 });
//             }
//           );
//         }
//       );
//     }
//   });
// });

// router.post('/reset/:resetKey', (req, res) => {
//   if (req.isAuthenticated()) {
//     return res.status(400).send({ message: 'Already logged in' });
//   }

//   const password =
//     req.body.password_first === req.body.password_second
//       ? req.body.password_first
//       : false;
//   if (password === false) {
//     return res.status(400).send({ message: "Passwords didn't match" });
//   }

//   models.User.findOne({
//     where: { resetPasswordKey: req.params.resetKey, verified: true }
//   })
//     .then(user => {
//       if (user === null) {
//         return res.status(404).send({ message: 'User no found' });
//       }

//       models.User.resetPassword(
//         user.email,
//         password,
//         req.params.resetKey,
//         (err, user) => {
//           if (err !== null) {
//             return res.status(400).send(err);
//           }

//           return res
//             .status(200)
//             .send({ message: 'Password successfully changed' });
//         }
//       );
//     })
//     .catch(error => res.status(400).send(error));
// });

// router.get('/reset/:resetKey', (req, res) => {
//   if (req.isAuthenticated()) {
//     return res.status(400).send({ message: 'Already logged in' });
//   }

//   models.User.findOne({
//     where: { resetPasswordKey: req.params.resetKey, verified: true }
//   })
//     .then(user => {
//       if (user === null) {
//         return res.status(404).send({ message: 'User no found' });
//       }

//       return res.status(200).send({ message: 'User exists' });
//     })
//     .catch(error => res.status(400).send(error));
// });

// router.post('/reset', (req, res) => {
//   if (req.isAuthenticated()) {
//     return res.status(400).send({ message: 'Already logged in' });
//   }

//   return models.User.findOne({
//     where: { email: req.body.email, verified: true }
//   }).then(user => {
//     if (user === null) {
//       return res.status(404).send({ message: 'User no found' });
//     }

//     models.User.setResetPasswordKey(user.email, (err, user) => {
//       let mailer = res.locals.mailer;
//       let config = res.locals.app.get('config');
//       let dev_mode = res.locals.app.get('env') === 'development';
//       var restoreUrl = config.frontend.password_reset_uri;

//       if (err !== null) {
//         return res.status(400).send(err);
//       }

//       restoreUrl = restoreUrl + user.resetPasswordKey;

//       mailer.send(
//         'email_reset',
//         {
//           to: dev_mode ? config.frontend.debug_mail : user.email,
//           subject: 'A Daily Clock password reset',
//           link: restoreUrl
//         },
//         err => {
//           if (err) {
//             console.log(err);
//             return res.status(400).send('There was an error sending the email');
//           }
//           return res.status(200).send({ message: 'Mail send' });
//         }
//       );
//     });
//   });
// });

// router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {

//   models.User.find({where: {id: req.user.id}}).then(user => {
//     if (!user) {
//       return res.status(404).send({message: 'User no found'})
//     }
//     res.status(200).send({
//       email: user.email,
//       verified: user.verified,
//       firstName: user.firstName,
//       surName: user.surName,
//       address: user.address,
//       postalCode: user.postalCode,
//       city: user.city,
//       phone: user.phone,
//       country: user.country,
//       membership: user.membership,
//     })
//   }).catch(error => res.status(400).send(error))

// })

// router.get('/logout', passport.authenticate('jwt', { session: false }), (req, res) => {
//   if (req.isAuthenticated() === false) {
//     return res.status(400).send({message: 'Not logged in'});
//   }
//   req.logout();
//   return res.status(200).send({message: 'Logged out'});
// })

// router.post('/contact', (req, res) => {
//   let user = req.isAuthenticated() ? req.user : null;

//   if (user !== null) {
//     req.body.user_id = user.id;
//   };

//   return models.Message
//   .build(req.body)
//   .save()
//   .then(message => res.status(201).send({message: 'Message send'}))
//     .catch(error => res.status(400).send(error));

// })

// router.post('/login', (req, res) => {
//   let passport = res.locals.passport;
//   let jwtOptions = res.locals.app.get('jwtOptions');

//   return passport.authenticate('local', { session: false },
//     (err, user, info) => {
//       return err
//         ? res.status(400).send(err)
//         : user
//           ? req.logIn(user, (err) => {
//             if (err) {
//                 return res.status(400).send(err)
//             }
//             let payload = {id: user.id};
//             let token = jwt.sign(payload, jwtOptions.secretOrKey);
//             return res.status(200).send({message: "ok", token: token});
//           })
//           : res.status(400).send(info);
//     })(req, res);

// });

// module.exports = router;