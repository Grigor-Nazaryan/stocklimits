import { Project } from '../db/models';
import uuidv4 from 'uuid/v4';

/**
 * @description Project Controller
 * @class ProjectController
 */
class ProjectController {
  /**
   * @description Create project
   * @param {object} req
   * @param {object} res
   * @returns {object} addProject
   * @memberof ProjectController
   */
  static async addProject(req, res) {
    try {
      const {
        platform,
        project_url,
        project_username,
        project_password,
        isActive,
        notify,
        frequency
      } = req.body;
      const { user } = req.authorize;
      const { userId } = user;
      const project = await Project.create({
        projectId: uuidv4(),
        userId,
        platform,
        project_url,
        project_username,
        project_password,
        isActive,
        notify,
        frequency
      });
      if (!project_url.includes(platform)) {
        return res.status(500).json({
          status: 500,
          data: `Project URL should begin with  https://www.${platform}.com/projects/`
        });
      }

      return res.status(201).json({
        status: 201,
        data: project
      });
    } catch (err) {
      return res.status(500).json({
        status: 500,
        data: err.message
      });
    }
  }

  /**
   * @description Update project
   * @param {object} req
   * @param {object} res
   * @returns {object} updateProject
   * @memberof ProjectController
   */
  static async updateProject(req, res) {
    try {
      const { projectId } = req.params;
      const {
        platform,
        project_url,
        project_username,
        project_password,
        isActive,
        notify,
        frequency
      } = req.body;
      await Project.update(
        {
          platform,
          project_url,
          project_username,
          project_password,
          isActive,
          notify,
          frequency
        },
        {
          where: {
            projectId
          }
        }
      );

      return res.status(201).json({
        status: 201,
        data: 'Project has been updated'
      });
    } catch (err) {
      return res.status(500).json({
        status: 500,
        data: err.message
      });
    }
  }

  /**
   * @description Delete project
   * @param {object} req
   * @param {object} res
   * @returns {object} updateProject
   * @memberof ProjectController
   */
  static async deleteProject(req, res) {
    try {
      const { projectId } = req.params;
      await Project.destroy({
        where: {
          projectId
        }
      });

      return res.status(201).json({
        status: 201,
        data: 'Project has been deleted'
      });
    } catch (err) {
      return res.status(500).json({
        status: 500,
        data: err.message
      });
    }
  }
}

export default ProjectController;
