module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      userId: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      lastname: {
        type: Sequelize.STRING,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      // activationKey: {
      //   type: Sequelize.STRING,
      //   allowNull: true,
      // },
      // resetPasswordKey: {
      //   type: Sequelize.STRING,
      //   allowNull: true
      // },
      isVerified: {
        type: Sequelize.STRING,
        //allowNull: false,
        defaultValue: false
      },
      // role: {
      //   type: Sequelize.STRING,
      //   allowNull: false,
      // },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: queryInterface => queryInterface.dropTable('Users')
};