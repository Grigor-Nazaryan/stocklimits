module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project', {
    projectId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    userId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    platform: {
      type: DataTypes.STRING,
      allowNull: false 
    },
    project_url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    project_username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    project_password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    notify: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    frequency: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  Project.associate = (models) => {
    // associations can be defined here
    Project.belongsTo(models.User, {
      foreignKey: 'userId',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',

    })
  };
  return Project;
};