const passportLocalSequelize = require('passport-local-sequelize');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      userId: {
        type: DataTypes.STRING,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {notEmpty: true},
      },
      lastname: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {isEmail: true},
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      // activationKey: {
      //   type: DataTypes.STRING,
      //   allowNull: true,
      // },
      // resetPasswordKey: {
      //   type: DataTypes.STRING,
      //   allowNull: true,
      // },
      isVerified: {
        type: DataTypes.STRING,
        //allowNull: false,
        defaultValue: false
      },
      // role: {
      //   type: DataTypes.STRING,
      //   allowNull: false,
      //   defaultValue: false,
      // },
    },
    {}
  );
  User.associate = models => {
    // associations can be defined here
    User.hasMany(models.Project, {
      foreignKey: 'userId',
      as: 'project'
    });
  };
  // passportLocalSequelize.attachToUser(User, {
  //   usernameField: 'username',
  //   hashField: 'password',
  //   saltField: 'salt',
  // });
  // passportLocalSequelize.attachToUser(User, {
  //   usernameField: 'email',
  //   activationRequired: true,
  //   activationkeylen:  8,
  //   resetPasswordkeylen:  8,
  // });
  return User;
};
