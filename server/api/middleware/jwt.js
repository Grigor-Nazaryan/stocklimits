import jwt from 'jsonwebtoken';

/**
 * @description JWT
 * @class JWT
 *
 */

class JWT {
  /**
   * @description Check token
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object} Error / Response
   * @memberof JWT
   */
  static authToken(req, res, next) {
    const token = req.headers.authorization;
    console.log(token);
    if (token === undefined) {
      return res.status(403).json({
        status: 403,
        data: 'No token found'
      });
    }

    req.token = token;
    return next();
  }

  /**
   * @description Generate token
   * @param {object} payload
   * @returns {object} Error / Response
   * @memberof JWT
   */
  static generateToken(payload) {
    return jwt.sign(payload, '1234', { expiresIn: 360000 });
  }

  /**
   * @description Verify token
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object} Error / Response
   * @memberof JWT
   */
  static verifyToken(req, res, next) {
    jwt.verify(req.token, '1234', (err, authorize) => {
      if (err) {
        return res.status(400).json({
          status: 400,
          data: 'Invalid token'
        });
      }

      req.authorize = authorize;
      return next();
    });
  }
}

export default JWT;
