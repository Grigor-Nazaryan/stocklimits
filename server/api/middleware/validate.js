import { Project } from '../db/models';

/**
 * @description Validating
 * @class ValidateProject
 *
 */
export default class ValidateProject {
  /**
   * @description Check if project exists
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object} Project
   * @memberof ProjectController
   */
  static async checkifProjectExists(req, res, next) {
    const { projectId } = req.params;

    const project = await Project.findOne({
      where: {
        projectId
      },
      attributes: ['projectId', 'userId']
    });

    if (!project) {
      return res.status(404).json({
        status: 404,
        data: 'Project does not exists'
      });
    }

    req.project = project;
    return next();
  }

  /**
   * @description Check if User
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object} updateProject
   * @memberof ProjectController
   */
  static async checkifUser(req, res, next) {
    const project = req.project;
    const { user } = req.authorize;
    const { userId } = user;

    if (userId !== project.userId) {
      return res.status(403).json({
        status: 403,
        data: 'You don\'t have permission'
      });
    }
    return next();
  }
}
