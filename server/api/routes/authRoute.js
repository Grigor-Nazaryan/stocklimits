import express from 'express';
import authController from '../controllers/authController';
import authValidation from '../validations/authValidation';
import jwt from '../middleware/jwt';

const { signupValidation, loginValidation } = authValidation;
const { authToken, verifyToken } = jwt;

const router = express.Router();

router.post('/register', signupValidation, authController.register);
router.post('/login', loginValidation, authController.login);
router.put(
  '/edituser/:userId',
  authToken,
  verifyToken,
  signupValidation,
  authController.updateUser
);

export default router;
