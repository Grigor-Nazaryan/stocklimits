import express from 'express';
import projectController from '../controllers/projectController';
import jwt from '../middleware/jwt';
import validate from '../middleware/validate';

const router = express.Router();

const { addProject, updateProject, deleteProject } = projectController;
const { authToken, verifyToken } = jwt;
const { checkifProjectExists, checkifUser } = validate;

router.post('/project', authToken, verifyToken, addProject);
router.put(
  '/project/:projectId',
  authToken,
  verifyToken,
  checkifProjectExists,
  checkifUser,
  updateProject
);
router.delete(
    '/project/:projectId',
    authToken,
    verifyToken,
    checkifProjectExists,
    checkifUser,
    deleteProject
  );

export default router;
