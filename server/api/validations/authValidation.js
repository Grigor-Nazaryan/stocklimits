import validator from 'validator';

/**
 * @description Authentication Validation
 * @class authValidation
 */
class authValidation {
  /**
   * @description Signup Vaalidation
   * @static
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object} validation
   * @memberof authValidation
   */
  static signupValidation(req, res, next) {
    let { name, lastname, email, password } = req.body;
    const spaces = /\s/g;
    const errors = {};

    // Remove white spaces
    name = name.replace(spaces, '');
    lastname = lastname.replace(spaces, '');
    email = email.replace(spaces, '');
    password = password.replace(spaces, '');

    if (!validator.isLength(name, { min: 5, max: 15 })) {
      errors.name = 'Name should be between 5 and 10 characters';
    }

    if (!validator.isLength(lastname, { min: 5, max: 15 })) {
      errors.lastname = 'Lastname should be between 5 and 10 characters';
    }

    if(!validator.isAlpha(name)){
      errors.name = 'Name should be an alphabet';
    }

    if(!validator.isAlpha(lastname)){
      errors.lastname = 'Lastname should be an alphabet';
    }

    if (!validator.isEmail(email)) {
      errors.email = 'Please put in a valid email';
    }

    if (!validator.isLength(password, { min: 5, max: 10 })) {
      errors.password = 'Password should be between 5 and 10 characters';
    }

    // Check if error
    if(Object.keys(errors).length === 0){
      const user = {}
      user.name = name
      user.lastname = lastname
      user.email = email
      user.password = password
      
      req.user = user
      return next()
    }

    return res.status(422).json({
      status: 422,
      data: errors
    })
  }

  /**
   * @description Login Vaalidation
   * @static
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object} validation
   * @memberof authValidation
   */
  static loginValidation(req, res, next) {
    let { email, password } = req.body;
    const spaces = /\s/g;
    const errors = {};

    // Remove white spaces
    email = email.replace(spaces, '');
    password = password.replace(spaces, '');

    if (!validator.isEmail(email)) {
      errors.email = 'Please put in a valid email';
    }

    if (!validator.isLength(password, { min: 5, max: 10 })) {
      errors.password = 'Password should be between 5 and characters';
    }

    // Check if error
    if(Object.keys(errors).length === 0){
      const user = {}
      user.email = email
      user.password = password

      req.user = user
      return next()
    }

    return res.status(422).json({
      status: 422,
      data: errors
    })
  }
}

export default authValidation;
